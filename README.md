WordPress ACF Shopify Field
=================

This will override an ACF select field with your stores product list. 

This assumes you are using the ACF options page plugin with a select field named "featured_product". 

To use this:

1. Go to your shopify store and create a private app. Copy the URL it gives you and paste it in class-shopify-widget.php in the private $url var (there is a comment telling you where to paste it).

2. Create an advance custom field (select field) and name it "featured_product" and assign it to the options page.

3. WHen you visit the options page the script will make a call to the shopify API and display all the products in your shop in the select menu. 

4. Pick a product to display and click save. This will make another request to the API for the product you select and then cache it in your themes template directory.

5. Use the class like so:

```php
$shopify = new ShopifyWidget();

$featured_product = $shopify->showFeaturedProduct();

var_dump($featured_product);
```

That's basically it. Your product will always pull form the cached file. The API requests will only be made when you load your options page in the back end and when you update your featured product. If you make changes to your product in the store you will want to update your option page since it will be using the cached version until you do. 

