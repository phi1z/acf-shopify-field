<?php
require_once ('lib/phpfastcache.php');
require_once ('lib/wcurl.php');

class ShopifyWidget
{
    
    function __construct() 
    {
        $this->addActionHooks();
    }
    
    private $url = ''; // ex: https://asdfsdf98hsf98ahsfasdfa:349hwaeueafosfnsaldfnas@my-store.myshopify.com/admin
    
    private function grabAllProducts() 
    {
    
        $r = array();
        
        $r['query'] = '/products.json?fields=id,title';
        $r['call'] = $this->url . $r['query'];
        $r['result'] = wcurl('GET', $r['call']);
        
        $result = $r['result'];
        
        return $result;
    
    }
    
    private function grabFeaturedProduct($field) {
        
        $cache = $this->setupCache();
        
        $selected_product = get_field('featured_product', 'options');
        
        $r = array();
        $r['query'] = '/products/' . $selected_product . '.json?fields=id,title,images,handle,variants';
        $r['call'] = $this->url . $r['query'];
        $r['result'] = wcurl('GET', $r['call']);
        
        $result = $r['result'];
        
        $cache->set("featured", $result, 999999999);
    
    }
    
    public function setupCache() {
    
        $config = array("storage" => "files", "path" => TEMPLATEPATH);
    
        phpFastCache::setup($config);
    
        $cache = phpFastCache();
    
        return $cache;
   
    }
    
    public function listAllProducts() {
    
        $products = $this->grabAllProducts();
    
        return $products;
    
    }
    
    public function listFeaturedProduct() {
    
        $cache = $this->setupCache();
    
        $featured_product = $cache->get("featured");
    
        return $featured_product;
    
    }
    
    public function loadShopProducts($field) {
    
        $field['choices'] = array();
    
        if ($this->listAllProducts()):
    
            $products = json_decode($this->listAllProducts(), true);
    
            foreach ($products["products"] as $choice) {
                $value = $choice['id'];
                $label = $choice['title'];
                $field['choices'][$value] = $label;
            }
    
        endif;
    
        return $field;
   
    }
    
    public function cacheOnSave($post_id) {
   
        if (get_field('featured_product', 'options')) {
   
            $field = get_field('featured_product', 'options');
            $this->grabFeaturedProduct($field);
   
        }
   
    }
    
    public function showFeaturedProduct() {
   
        if ($this->listFeaturedProduct()):
   
            $product = json_decode($this->listFeaturedProduct());
            return $product->product;
   
        endif;
   
    }
    
    public function addActionHooks() {
   
        add_filter('acf/load_field/name=featured_product', array($this, 'loadShopProducts'));
        add_action('acf/save_post', array($this, 'cacheOnSave'), 20);
   
    }

}
